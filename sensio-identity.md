# About Sensio Identity (SensioID)

> SensioID is a hybrid [DID](definitions.md#did) and KeyManager fot (de)centralized PKI.

# must read

- https://github.com/decentralized-identity/sidetree/blob/master/docs/protocol.md
- https://github.com/blockstack/blockstack-core/blob/master/docs/blockstack-did-spec.md
- https://www.npmjs.com/package/distributed-ids
- https://w3c-ccg.github.io/ld-cryptosuite-registry/
- https://github.com/decentralized-identity/element
- https://element-did.com/server/info
- https://w3c-ccg.github.io/did-spec/
- https://opencreds.org/specs/source/identity-credentials/#expressing-an-identity
- https://browser.blockstack.org/

My DID on ELEMENT-did.com

`did:elem:BMsJRQxYaZyjlryswMM18XLrCcqZbdLkd68s8Lrj_tA`

My DID on blockstack
`woss.id.blockstack` and `ID-1H291YHCzQFq369ggaxdUtU3bGUtZvFgzV`
