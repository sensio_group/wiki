The decentralized protocol for proving multimedia and content [ownership](https://en.wikipedia.org/wiki/Ownership), its discovery, timestamping and monetization.

Network of notarized proofs of verifiable [exclusive rights](https://en.wikipedia.org/wiki/Exclusive_right), [copyrights](https://en.wikipedia.org/wiki/Copyright) and their [usage](https://en.wikipedia.org/wiki/License).

Using digital signatures and blockchain technology, SensioNetwork empowers multimedia content creators to sign, permanently record, and claim statements about their ownership and copyrights.

SensioNetwork aims to provide:

- an easy and flexible way to store the ownership information
- verify ownership photographs through the workflows
- irrefutable proof of ownership
- publicly available proof of ownership
- smart contracts which can be used to monetize the owned photos

## Blockchain

## How does it work

To deliver the verifiable web to content creators and consumers, SensioNetwork leverages four important technologies:

- Content hashing
- Cryptographic Digital Signatures
- Decentralized Storage
- Blockchain Anchoring

## Technologies

### Content Hashing

Cryptographic hashing functions provide the means to irrefutably identify content. Given the same input, a hashing function will always produce the same output (the hash), so a content creator can use the hash to identify the created work. A hash can be generated for a file of any size, like a fingerprint, the hash output will always be relatively small. The small size makes hashes a convenient and economical substitute for the file contents when all we care about is uniquely identifying the content.

### Cryptographic Digital Signatures

Cryptographic Digital signatures enable content creators to prove ownership of a piece of content, or another party's right to license it (a License Claim), or any number of other possible claims. Signatures provide cryptographic proof of who issued a claim. Every claim in SensioNetwork is signed by its issuer, which means a random user can't publish claims on behalf of another user. Claims include the hash of the claim's subject along with other information provided by the claim creator. Combined with content hashing, digital signatures provide cryptographic proof of who said what, and the subject the claim is about.

### Decentralized Storage

SensioNetwork claims are recorded on IPFS, a peer-to-peer network for distributing content in a decentralized manner. Data on IPFS is stored on multiple computers owned by many different operators. By leveraging this decentralized storage model, the claims are distributed across a wide range of IPFS network participants, making the removal of any of those records extremely difficult.

### Blockchain Anchoring

All claims are recorded (we use the term 'anchored') on the [Sensio Blockchain](definitions.md#sensio-blockchain) batching multiple claim IDs to single transaction or writing each claim as separate transaction. Once the block is created, all claims are irreversible.

### Revoking claims
