# Web developer needed to help us build a next-gen privacy-oriented web app

Looking for a web developer for a 6-month project. You will work with a small international team for this project and on multiple sup-projects.

We are building an application for multimedia copyright which will allow anyone to protect their content, obtain proofs of their ownership and copyright using our Sensio Network blockchain.

Required Experience (must have):

- typescript in both front-end and back-end environment
- react js (SPA, SSR, PWA)
- HTML5/CSS3
- yarn, git, Lerna
- writing unit tests using jest
- Semantic-UI and MaterialUI CSS frameworks
- familiarity with Oauth2, passport.js, Auth0
- UI/UX interactivity design

Required Experience (optional, will be a +)

- familiarity with Docker
- PostgreSQL database knowledge
- CI/CD workflows,
- GraphQL
- image processing libraries like (sharp, imageOptim, ImageMagick)
- experience with blockchain and DApps
- experience with Substrate based chains
- additional languages (Rust, Python)

To be the best fit for this project you need to:

- communicate clearly
- be self-motivated and self-organized
- take charge, ownership, and responsibilities of the tasks you do
- be a team player
- enjoy taking photos and share them
- care about data privacy
- write readable, robust and maintainable code
- actively participate in design and architecture decisions

If you are interested in learning more about the project itself check out https://sensio.group

Price and time

- 30 h/week or less
- 20000 EUR for 6 month project
