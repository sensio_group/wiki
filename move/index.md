## How to write the wiki

This wiki is written in [Markdown](https://www.markdownguide.org/getting-started/), a lightweight markup language that you can use to add formatting elements to plaintext text documents.

Getting Started with [markdown](https://www.markdownguide.org/getting-started/) and [with diagrams in markdown](https://mermaidjs.github.io/#/)

## Contribution Workflow

The following flow is used, for now.

```mermaid
graph LR
    id1[Create an Issue] --> id2[Create Merge Request] --> id3[Do the stuff] --> id4[Commit/Push] --> id5[Merge]
```

## Get Support

[General Chat](https://discord.gg/UYhBDRy)

## Security

Find out how to [report security issues or bugs](security.md) to Sensio development team.

## Links

- <https://github.com/mgmtio/video-phash-service>
- <https://sofa-api.com/>
- <https://geekflare.com/nodejs-security-scanner/>
- <https://dask.org/> _machine learning_
- <https://democracy.earth/>
- <https://population.un.org/wpp/>
- <https://www.mycryptopedia.com/ewasm/>
- <https://akropolis.io/#products>

```typescript
// https://opentimestamps.org/info/?004f70656e54696d657374616d7073000050726f6f6600bf89e2e884e892940108c349b4296015ff49ca89511eedde9c98a05a9edf12f83f8f1a5af2cd08c14b2af010be1724df288ae7025aa9c5e129345d8d08fff01015657532cd9cbf1feb475f9519ab184508f1045cefd262f0088b3fe46a0b71737c0083dfe30d2ef90c8e2e2d68747470733a2f2f616c6963652e6274632e63616c656e6461722e6f70656e74696d657374616d70732e6f7267fff010b96dd2512a8d459285a5aa336540080c08f0107a09e0ec0b97b2d2d33603c175e0e1d808f1045cefd262f0080fdd8dd5f42275de0083dfe30d2ef90c8e292868747470733a2f2f66696e6e65792e63616c656e6461722e657465726e69747977616c6c2e636f6dfff010d88c1d4469b025652310c56edc9f054508f1045cefd263f0081638f522af89d2cb0083dfe30d2ef90c8e232268747470733a2f2f6274632e63616c656e6461722e636174616c6c6178792e636f6df010db28532737c71b0fadd81f4f6296c71c08f1045cefd262f00877d36b76689639470083dfe30d2ef90c8e2c2b68747470733a2f2f626f622e6274632e63616c656e6461722e6f70656e74696d657374616d70732e6f7267
interface CameraContentPayload {
  maker: string; // xmp:Make
  model: string; //xmp:Model
  serialNumber: string; // xmp:SerialNumber
}
function generateCameraContentHash(payload: CameraContentPayload) {
  contentHash = sha3(payload);
}
```

## Future

- [ ] crypto Keys for every person, wide adoption
- [ ] promote FIDO hard keys like Yubikey
- [ ] integrations with Identity Provider services like Auth0/Blockstack
- [ ] verifiable human through Keybase
- [ ] verifiable claims of ownerships/rentals/purchases
- [ ] distributed and publicly available proof of claims
- [ ] privacy/security/transparency by default
- [ ] users own their data
- [ ] full encryption of every item
- [ ] DApp
- [ ] bring your own storage

![User SOF](assets/user-sof-camera-flow.png)

## Etymology and definition

> Noun
>
> [sēnsiō](<(https://en.wiktionary.org/wiki/sensio)>) (genitive sēnsiōnis); third declension
>
> (rare) thought

History and Etymology for thought

> Middle English, from Old English thōht;
