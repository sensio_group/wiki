# Creative Work Claim Type

The Creative Work claim type is a backbone of the SensioID claims ecosystem. Most other claims in the ecosystem will reference a creative work. The most generic kind of creative work, including books, movies, photographs, software programs, etc.

The purpose of the Creative Work claim type is to standardize the format for all the claims, to anchor the data to blockchain and give insight into owners rights and restrictions

## Properties

- `hash`: String [Content ID](https://github.com/multiformats/cid) String - A one-way, deterministic cryptographic hash of the claim payload.
- `type`: String - The type of the creative work. Options: [ownership]
- `author`: [Data URI](https://en.wikipedia.org/wiki/Data_URI_scheme)- A reference to the author of the creative work.
- `contributors`?: Array of URIs - References to contributors. -- @TODO
- `tags`?: Array of Strings - Indexable keywords related to the work.
- `license`?: URI - A reference to license terms granted to users of the creative work.
- `dateCreated`?: ISO 8601 String - The date the work was created on.
- `canonicalUrl`?: URL String - An "official" location to be used as a homepage for the Creative Work in links or citations.

### Example

```json
{
  "hash": "8tUTPu1naqEtYR9Z52df1DqMejKc1yWD1AxrV6Vu7FBu14cLwJc5jmaeMsxgNTcmuW5ksXddM4G7jZoCyCVcquMsMC",
  "license": "https://license.uri.which.can.not.be/changed",
  "author": "https://woss.sensio.id/",
  "canonicalUrl": "https://explorer.sensio.id/claims/8tUTPu1naqEtYR9Z52df1DqMejKc1yWD1AxrV6Vu7FBu14cLwJc5jmaeMsxgNTcmuW5ksXddM4G7jZoCyCVcquMsMC",
  "dateCreated": "2019-01-02T00:00:00.000Z"
}
```
