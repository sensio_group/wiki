# Ownership Claim Type

Ownership claim type extends the Creative Work Claim type.

## Properties

- `shares`: [String](../glossary.md#shares) - Conditions of the ownership.
- `type`: String - Value is `ownership`

### Example

Alice owns the **Canon 750D** camera and the **Canon 18m-135m IS USM** lens that she uses to take most of the pictures.
She used the [SOP](../glossary.md#sensio-ownership-flow) to verify the fact that she owns the equipment.
SensioID generates the claim of the ownership with the 100% ownership.

```json
{
  "hash": "8tUTPu1naqEtYR9Z52df1DqMejKc1yWD1AxrV6Vu7FBu14cLwJc5jmaeMsxgNTcmuW5ksXddM4G7jZoCyCVcquMsMC",
  "author": "https://alice.sensio.id/",
  "canonicalUrl": "https://explorer.sensio.id/claims/8tUTPu1naqEtYR9Z52df1DqMejKc1yWD1AxrV6Vu7FBu14cLwJc5jmaeMsxgNTcmuW5ksXddM4G7jZoCyCVcquMsMC",
  "dateCreated": "2019-01-02T00:00:00.000Z",
  "type": "ownership",
  "shares": [{ "unit": "percentage", "symbol": "%", "value": 100 }]
}
```

![user sof camera](../../../assets/user-sof-camera-flow.png)
