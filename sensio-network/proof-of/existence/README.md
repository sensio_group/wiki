# Proof-of-Existence

The goal of POE pallet is to create a verifiable proof for incoming data. We can do that by providing a set of rules and applying them on the data, then taking the output and creating the proof.
Each rule contains a list of operations where each operation produces a single hash.
Now let's have a closer look into how the Rules work.

## Rule

A **Rule** is a defined structure which we use to generate a `Proof`.

Rules are Sensio's approach to create a standardized way of generating self-describing proofs. Each of the proofs is a combination of the operation outputs and the rule type.

#### Invalidations / Replacing / Updates

There will be a time when a rule must be invalidated and replaced. The way how we accomplish that is through a `parent` field.

When we derive new rule from another rule, `version` field must be incremented by `1` and all operations need to be saved with the new rule. Let's say that we have a saved rule with 3 operations and we want to add few more operations. The ONLY way is to create new rule with ALL of the operations that we want to keep from original rule plus operations we want to add.

A rule cannot be derived from a different rule type. For example, we cannot create a new rule with the type `Photo` from the a rule that has the `Lens` type. The two are not compatible as they work with different sets of data.

#### Example

Implementing the same rule on top of the same data will always give us the same proof. While implementing the same rule on a similar data will provide a similar proof.

If you find this confusing, check the definition of a proof [here](#proof).

We can use this approach to compare generated proofs and to look for the similarities. Excellent example is proof generation for a Photo. Check it out [here](#proof).

To be a valid rule, the `ops` field must have at least one operation.

If you are interested in example check out the [tutorial](/tutorials)

Generic Rule structure:

```ts
// Types
enum ForWhat {
  Generic,
  Photo,
  Camera,
  Lens,
  SmartPhone,
}

// Content identifier
type CID = string;

// Generic type ALL identifiers have
type PoeId = CID;

// Generic user identifier, for now PoeId but in the future fully fledged SSI
type CreatorId = PoeId(UUID | URN | SSI | ANY);

// Rule interface
interface Rule {
  id: PoeId;
  data: {
    version: number;
    name: string;
    desc: string;
    creator: CreatorId;
    forWhat: ForWhat;
    parent: PoeId;
    ops: [Operation];
  };
}
```

## Operation

Is an action that has a defined structure and always returns [CID](/glossary.md#cid). The operation cannot access the output of another operation of the same level, it only processes the outputs of the operations it depends on. That would be the case for a complex operation, where a single operation contains one or more operations that are needed to produce the final output.

Generic structure:

```ts
// Params specifics for the op
interface Params {
  k: string;
  v: string;
}

// Operation interface
interface Operation {
  id: PoeId;
  data: {
    op: string;
    desc: string;
    params?: [Params];
    hashing: {
      algo: string;
      bits: number;
      skip: boolean;
    };
    encoding: {
      algo: string;
      prefix: boolean;
    };
    ops: [Operation];
  };
}
```

In the case of simple operations the _ops_ field is an empty array, in the case of the complex one the execution would start from the _deepest_ op in the _ops_ and executed upwards implementing the same algorithm.

In order to produce the CID, we need to define the cryptographic hashing algo which is needed to create multihash. Sensio Network defaults to algo `blake2b` with `256` bits in length. This cannot be changed for existing operations, but it gives the freedom to the user(a developer) to choose their algorithm and bits as long they are defined as valid multihash inputs.

### Operation nesting

Take a look at the Operation definition below, you will notice a parameter definition `ops: [Operation]`, it says that parameter `ops` is a list of `Operation`, it can be an empty list too, in that case it contains 0 operations.

In this case we create complex operations where the goal is to generate a single value that is derived from the dependent (child) operations.

The definition states that each operation output value MUST be a CID string generated using the `hashing` and `encoding` definitions and each operation can perform a **single** action (extract serial number from the photo metadata) we must be able to add more operations if we wanted to do any other operation that parent operation requires as an input.

Example:

We are building an operation called `combo_copyright_camera_id`.
We need to do the following:

1. extract copyright from the metadata
2. extract camera id from the metadata

```ts
// Defaults
const hashing = {
  algo: 'blake2b',
  bits: 256,
  skip: false, // this tells the operation executor to skip hashing
};
const encoding = {
  algo: 'hex',
  prefix: true,
};

// Created by Sensio, comes preinstalled
const metaSerialNumber = {
  id: 'bafk2bzacedmu4k7cl6jo7vtkbpduhtx2ilkm7qcqmc4xizy5kfzoiisn4eyrm',
  data: {
    op: 'meta_serial_number',
    desc: 'Extract SerialNumber from Metadata',
    hashing: hashing,
    encoding: encoding,
    ops: [],
  },
};

// Created by Sensio, comes preinstalled
const metaCopyright = {
  id: 'bafk2bzacecrub2yp6vkmmquufivmcx63nzvz4py44bih7w5gdmnahqbzvvveu',
  data: {
    op: 'meta_copyright',
    desc: 'XMP copyright field',
    hashing: hashing,
    encoding: encoding,
    ops: [],
  },
};

// Our custom operation
const customOperation = {
  op: 'combo_copyright_camera_id',
  desc: 'Create unique identifier that links copyright and camera serial number',
  hashing: hashing,
  encoding: encoding,
  ops: [metaCopyright, metaSerialNumber],
};
```

Using the UI or the API we can save this operation to Sensio Network. If it is a success we will get the ID, a content address that describes the operation. The ID is not randomly generated, it is derived from the operation itself. The operation looks like this when it retrieved from the chain.

```ts
const operation = {
  id: 'bafk2bzacebcttrvsschcj6m2t3eoo6evtanrujwzv7sk5jrdablwiyznm3s5m',
  data: {
    op: 'combo_copyright_camera_id',
    desc: 'Create unique identifier that links copyright and camera serial number',
    hashing: {
      algo: 'blake2b',
      bits: 256,
      skip: false,
    },
    encoding: {
      algo: 'hex',
      prefix: true,
    },
    ops: [
      {
        id: 'bafk2bzacecrub2yp6vkmmquufivmcx63nzvz4py44bih7w5gdmnahqbzvvveu',
        data: {
          op: 'meta_copyright',
          desc: 'XMP copyright field',
          hashing: hashing,
          encoding: encoding,
          ops: [],
        },
      },
      {
        id: 'bafk2bzacedmu4k7cl6jo7vtkbpduhtx2ilkm7qcqmc4xizy5kfzoiisn4eyrm',
        data: {
          op: 'meta_serial_number',
          desc: 'Extract SerialNumber from Metadata',
          hashing: hashing,
          encoding: encoding,
          ops: [],
        },
      },
    ],
  },
};
```

The `params` field can be used to tell the implementation on specifics. For example, in the case of generating a perceptual hash, we could use different algorithms, but we want to use the `blockhash` implementation. For this case that field would look something like this:

```ts
const params = [
  {
    k: 'algo',
    v: 'blockhash',
  },
];
```

### Execution

When the operation is executed _metaCopyright_ will execute first and _metaSerialNumber_ will execute second, each of them producing a CID value, creating `ops: ["bafk2b1234sda...", "bafk2b231123asda..."]`. Now this operation will execute itself and take the CIDs as input and generate a CID that identifies the executed output of its children and itself.

The full operation structure now looks like this:

```ts
// Our custom operation
const customOperation = {
  op: 'combo_copyright_camera_id',
  desc: 'Create unique identifier that links copyright and camera serial number',
  hashing: hashing,
  encoding: encoding,
  ops: ['bafk2b1234sda...', 'bafk2b231123asda...'],
};
```

What would be the execution order of the `customOperation` if the `metaSerialNumber` operation would have child operation in its `ops` field called `metaModel`?

```mermaid
graph LR
    metaCopyright --> metaModel --> metaSerialNumber --> CIDs
```

Child operation is any operation that is defined in the `ops` field. In our case the `customOperation` has 2 children operations `metaCopyright` and `metaSerialNumber`

> Each child operation will be checked for child operations until there are not children defined in `ops`, length of the `ops` is 0. Then the executor will go upwards and execute the operations building the same structure as before.

Example on how the depth goes, the execution order is always upward, from the lowest level to the highest.

```sh
| metadata_hash has 3 ops
  |___ meta_serial_number  has 2 ops
    |______ meta_make has 1 ops
      |_________ meta_create_date has 0 ops
    |______ meta_lens_model has 0 ops
  |___ perceptual_hash has 0 ops
  |___ meta_document_id has 0 ops
| raw_pixels_hash has 2 ops
  |___ perceptual_hash has 0 ops
  |___ meta_original_document_id has 0 ops
```

> Operation is a definition of the implementation. The implementation can be written in any language and executed anywhere.

## Proof

Is a structured final output of the rule. Each proof is different in a few aspects and similar in others.

Proof params is the collection of the operation outputs mapped via operation name. A `proofId` will always be different for different data, but not the proof params. That's because the proof params are controlled completely by the rule definition and its operations.

#### Invalidations / Replacing / Updates

We can invalidate the proof by generating a new one and setting the `prev` field to the previous `proofId` we want to invalidate. In order to get the last proof which is also the valid one we must look for the proof that is not linked to any other proof, in other words where `current.proofId` does not exist in the `anyProof.prev` field.

Generic structure:

```ts
// Generic type that is result of the single operation
interface ProofParams {
  k: string; // Operation.data.op field
  v: string; // function(Operation)
}

// Generic Proof
interface Proof {
  id: PoeId;
  data: {
    ruleId: PoeId;
    prev: PoeId;
    creator: CreatorId;
    forWhat: ForWhat;
    params: [ProofParams];
  };
}
```
