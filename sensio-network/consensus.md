# Consensus

Sensio Network uses nominated proof of stake (NPoS) as well as finality algorithm [GRANDPA](https://polkadot.network/polkadot-consensus-part-2-grandpa/) and a block production engine [BABE](https://polkadot.network/polkadot-consensus-part-3-babe/) developed by [Web3 Foundation](https://web3.foundation/)

As a [parachain](../definitions.md#parachain) on Polkadot network, Sensio has its shared security.

To revert the block on Sensio Network an attacker would have to revert the entire Polkadot system.

SN nodes are collecting transactions and producing state transition proofs for the the Relay chain validators. In the Polkadot ecosystem they are known as Collators. These are light nodes that do not require much power and can be run even on a smartphone.
