# Token

> [!NOTE]
> Feedback is welcome

Suggested name is `THT` as per [definition of Sensio word](readme.md#etymology-and-definition), full name would be `THOUGHT`.

`THT` is a utility token used in Sensio Network to pay for the network fees, user subscriptions and incentive layer.

Acquiring the THT token will be possible through the exchanges or as a reward for performed actions on the network.

Potential actions could be :

- [successful participation](definitions.md#successful-participation) in training the AI models for better image recognition
- selling the license/copyright/ownership ([p2p Market](p2p-marketplace.md))
- validating the network (yearly earning)
- host the data? (pinning in IPFS style)
