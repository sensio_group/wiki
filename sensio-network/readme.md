# About SensioNetwork (SN)

> [!NOTE]
> heavy WorkInProgress

Guidelines in writing:

- [ ] what is the blockchain part
- [ ] write about consensus
- [ ] pooled security (inherited by polkadot)
- [ ] proofs
- [ ] claims (ownership, copyright, license, rentals)
- [ ] transferring the rights
- [ ] timestamping
- [ ] decentralization / distributed (explain the validators ... collators)
- [ ] on-chain identity (similar to what polkadot has on the chain itself)
- [ ] governance
  - [ ] voting system (validators agreeing on new version of the chain and its rules)

## About

The decentralized protocol for creating multimedia and content [ownership](https://en.wikipedia.org/wiki/Ownership), its discovery, timestamping and monetization.

SensioNetwork is a decentralized protocol for creating multimedia and content [ownerships](https://en.wikipedia.org/wiki/Ownership), its discovery, ProofOfExistence and monetization.

SensioNetwork is a decentralized protocol that empowers content creators to sign, permanently record, and claim statements about their ownerships and copyrights, giving them opportunity to license their work and get paid.

Using digital signatures and blockchain technology, SensioNetwork empowers multimedia content creators to sign, permanently record, and claim statements about their ownership and copyrights, giving them opportunity to license their work and get paid.

SensioNetwork aims to provide:

- an easy and cheap way to store the ownership claims
- tamper-proof and publicly available ownership claims
- inherited copyrights for digital media (photos, videos, music)
- smart contracts for monetization
