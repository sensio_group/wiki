# Setup the environment (one time only)

Follow the installations instructions to set up the environment, then come back here and continue reading :blush:
Install these :arrow_down:

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [nodejs](https://nodejs.org/en/download/current/)
- [yarn package manager](https://legacy.yarnpkg.com/en/docs/install/)
- [how to enable SSH on windows 10](https://www.howtogeek.com/336775/how-to-enable-and-use-windows-10s-built-in-ssh-commands/)
- [GPG full suite](https://gpg4win.org/download.html)
- [Draw.io](https://github.com/jgraph/drawio-desktop/releases/tag/v13.0.3)

Any text editor will do for writing markdown, one of the most versatile is [Visual Code](https://code.visualstudio.com) with these plugins:

- [Markdown All In One Plugin](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
- [Markdown Mermaid preview for the diagrams](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid)
- [Emoji](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-emoji)
- [Grammarly](https://marketplace.visualstudio.com/items?itemName=znck.grammarly)

Lets verify your installation. Copy line by line from the following snippet. If there are errors please consult installation instructions.

```bash
git --version
# git version 2.25.0
node --version
# v13.7.0
```

### SSH and committing

We must have SSH keys in order to commit anything to the GitLab servers. They are our keys to the server with max security. If you have them go [here](https://gitlab.com/profile/keys) and add them to GitLab. Name them meaningfully, like `My main laptop key`.

Now you should be fine with committing to the repo via SSH, congrats :clap: :clap: :clap: !!!

### Signing Your Work

We require that every commit is signed with the PGP key.

Comprehensive guide is available on [official git pages](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work)
