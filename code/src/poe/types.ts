/* eslint-disable @typescript-eslint/camelcase */
export const PoECustomTypes = {
  Operation: {
    op: 'Vec<u8>',
    desc: 'Vec<u8>',
    hashAlgo: 'Vec<u8>',
    encodeAlgo: 'Vec<u8>',
    prefix: 'Vec<u8>',
    ops: 'Vec<Operation>',
  },
  Rule: {
    description: 'Vec<u8>',
    createdAt: 'Vec<u8>',
    creator: 'Vec<u8>',
    version: 'u32',
    forWhat: 'Vec<u8>',
    parent: 'Vec<u8>',
    buildParams: 'Operation',
    ops: 'Vec<Operation>',
  },
};

export default PoECustomTypes;
