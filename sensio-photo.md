# About Sensio Photo

all encrypted by default

# Discovery

since all content is encrypted by default we must ask the user to allow us to use certain information about the photo/video/text unencrypted for search and analytics.

# Challenges

## When the user shares the photo how do we tackle decryption?

One way is that it is encrypted with the pub key of the receiving user. If it is intended for multiple users then maybe use a shared key???

## Graphql With encrypted stuff

## How to solve the issue with 3rd party services getting the tags for encrypted photo

## Decrypting on the client-side and still use GraphQL
