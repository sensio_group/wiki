# SensioID API

We are using the [GraphQL](https://graphql.org/) to power all SensioID API requests. GraphQL is self describable, input safe query language, which means that compared to standard API (REST) developer doesn't need to look anywhere else for the API documentation (the API is a documentation) and be 100% sure what input and outputs are.

The Purpose of the API is to:

- serve as connection to the verifiable web
- allow developers (API Account Holders) to extend and connect to SensioID Network

## API access

### Token based

There is One token generated by the SensioID:

- API token (or API key) is used to access the SensioID network

### Oauth based

For the developer access and extending the SensioID Network we will be conforming with the [OAuth2.0](https://oauth.net/2/) specification and implementation of the Applications

## Tokens

Three tokens are send to clients:

- token for email verification
- JWT access token for logging in
- JWT refresh token for refreshing the access token

## Accounts

_TBD_

## Works

## Ownerships

This is probably the most common Claim type
