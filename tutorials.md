# Welcome to the Tutorial Section

> NOTE: This is a WIP and in a very early stage of the development. All the data will be eventually removed and under no circumstances proofs are legally binding. So don't share them and say that you have the proof of anything at this point.

> This demo does not require any actual money or transaction from your end, we use the DEV token and our actors Alice and Bob have substantial amount. Also the DEV token is useless outside of this demo.

[Sensio network Proof of Existence Tutorial](sensio-network/proof-of/existence/tutorial.md ':include')
