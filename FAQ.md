# New to Sensio? This is the place to start.

Answers to the most common questions is to help you get a big picture understanding of Sensio: what it is, what it solves, and how.

Note: Please note that Sensio is still in development and that some of the features mentioned herein have not yet been released. This guide will be continually updated.

## What is Sensio?

Sensio is a collection of services for content creators (for now, mainly, photographers) for:

- photo-management and sharing,
- provable copyright and ownerships (photos and equipment),
- licensing and selling creative works.

We create distributed open-source software for the digital photography market.
Our first step is to make sure photographers are the only ones in charge of the photos they share online and their data.

In the long run, Sensio's goal is to form the creative market where content creators are not bounded to any platform and can set the terms for others to use their work without a hitch. Where publishers, marketers and other creatives can acquire quality content directly from its authors in a few clicks.

## What is Sensio.Photo?

Sensio.Photo is an app where photographers can organize and manage their collections, interact with clients, set the terms for others to access and use their work, and much more.
Simply put, it's an easy-to-use content management tool with built-in copyright protection.


## What Sensio.Network is for?

Sensio.Network is the backbone of Sensio. It's a decentralized protocol built on [Substrate](https://substrate.dev/) that allows multimedia content creators to sign, permanently record, and timestamp statements about their copyright and ownership. 
Currently, ownership validation and workflows are available only for digital photographs and for the photo equipment.


## What Sensio does to Copyrights?

In photography, copyright is created with the click of a shutter. From the moment you took a photo, you own the copyright. In practice, however, it is rather tricky to keep track of your photos when they are shared (and re-shared) online. Images can be easily amended with just a color filter, a brightness adjustment or by cropping. To prove an infringement, you´ll need to bring the case to the court.
Today 'Owning copyright', does not necessarily equal 'being able to prove it'... and that's something we at Sensio want to change!

Our goal is to make copyrights easy to check and to prove. With photos, we make sure every rendition caries the current copyright.  
In other words, we create trackable records for an image and all its modifications as well as the history of licenses and a copyright transfer.


## How does Sensio know if a copyright claim is legit? 

Before anyone can claim copyright of a photo a user needs to go through the equipment/device verification workflow. Therefore, ownership of a physical asset needs to be proved first. Only when that is in place, a user can claim copyright of the photos created with that equipment.

This approach is what makes Sensio statements different from those created by time-stamping services. The latest confirm only the fact that a certain digital asset existed at a certain point in time providing no evidence whatsoever about whether a user uploading the content is a creator and a rightful owner.


## What does it have to do with the EU Copyright Directive?

According to the [EU Copyright Directive, Article 17](https://en.wikipedia.org/wiki/Directive_on_Copyright_in_the_Digital_Single_Market) (previously known as art.13), online platforms and aggregator sites will become liable for copyright infringements. EU member states have time till May 2021 to implement their solutions.
It means that websites will start blocking upload of any content that has at least some elements (images, audio, etc.) created by a third party unless there is a proof that the user has a right (respective license) to reuse that creative work. 
Sensio aims to automate the process of providing such proofs. Besides,  we also make it super easy and straightforward to get the licences needed.

## Who Sensio is for?
Sensio is for everyone who takes photos and shares them online. Photographers using professional equipment and software will make the most of the Sensio early version.

## How can I get involved?

If you are a photographers we would love to hear from you and learn more about your workflow.
Besides, Sensio Ambassador Programm is underway. Our ambassadors will be the first to get access to Sensio Beta and test it out.
Best way to get in touch is to join our [Discord server](https://discord.gg/WHe4EuY)

If you want to help us build Sensio check out our [development portal](https://wiki.sensio.dev/)
Or better still get in touch on [Discord](https://discord.gg/WHe4EuY)

If you want to know more, please check [Sensio wiki](https://wiki.sensio.dev/) 

To stay posted on our news [follow us on Twitter](https://twitter.com/sensio_group)
or subscribe to [Sensio blog](https://medium.com/sensio-group) (More to come soon)
